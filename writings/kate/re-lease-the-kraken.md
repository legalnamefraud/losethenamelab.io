---
layout: layouts/page.html
title: Release the Kraken
subtitle: by kate of gaia
titleCentered: true
tags: [writings]
excerpt: It is not a coincidence the the Perseid Meteor shower occurs in the month of Leo. It is also not a coincidence that I seemingly had the urge to watch "Clash of the Titans" again either. I have long said that the truth is being presented everywhere and none more so than the fantasy world of Hollywood. Once the human mind is conditioned into the "literal left" mind of thinking, those that wish to rule you have no fear of tossing truth in your face with a sneer, fulfilling the honour obligation of allowing your free will choice of ignorance to rule you. It is true, the deceivers are everywhere but those with eyes to see and ears to hear will see beyond that veil now, further than you have in your entire lifetimes. But, that too, is free will choice.
---

It is not a coincidence the the Perseid Meteor shower occurs in
the month of Leo. It is also not a coincidence that I seemingly
had the urge to watch "Clash of the Titans" again either. I have
long said that the truth is being presented everywhere and none
more so than the fantasy world of Hollywood. Once the human mind
is conditioned into the "literal left" mind of thinking, those
that wish to rule you have no fear of tossing truth in your face
with a sneer, fulfilling the honour obligation of allowing your
free will choice of ignorance to rule you. It is true, the
deceivers are everywhere but those with eyes to see and ears to
hear will see beyond that veil now, further than you have in
your entire lifetimes. But, that too, is free will choice.

Perseus, the demi-god, was at war with himself being of godly
conception and hating the god aspect within. His is a story of
coming to terms with that divine conception, embracing it
finally and then choosing to still be just a man, albeit, one in
only in partial control of his ego left Cain mind. Io, his most
faithful servant followed him his whole life, she was his pure
white feminine guardian. She is slain saving him and is thus
given back as a gift from the "gods" upon his attainment of his
own ascension once the human mind/ship helm was taken back and
under his now more benevolent control. He refuses "kingship-"
and the attached corruptibility of authority that inevitably
follows every man and woman who chooses to rule over others.
Rare are those that can lead versus rule.

The carnal left is rep-resented by Hades and the Kraken. If we
are at war within, we are at war without, as above, so below, as
within, so without. The inspiration of this now moment came in
the form of a meteor last night whereupon I remembered that this
is the peak time for the Perseid shower as we pass through the
tail debris of comet Swift-Tuttle at this time every year. When
one begins to really "see", that the clues, the correlations and
the "coincidences" are everywhere, you will begin to truly see
truth with no exceptions. Last night here was completely clear
after much rain fall and cloud cover. This brings to mind
another realm of possibility as to the further uses of
"chemtrails" amongst other “fogs of war”. If the "above" is
blurred, so goes our programmed “mineds”/minds. The joy in the
truth is that once something is “real-eyes-ed”, the spell is
broken and of no more effect on you.

Alas, I digress. Perseus was aided by Io, a mortal cursed with
forever youthfulness where she gets to watch those that she
loves die off as she will never age. Io is the creative right
virgin bride of thought. He is also aided by the Argos warriors,
all of whom with him, eventually perish but not before assisting
Perseus in many ways on their journey to save Andromeda, another
representative of the virgin thoughts and the kingdom of
Argos/the kingdom of heaven within....Ar-gent is French for
silver. Silver is the Moon, the reflections since no other metal
reflects light better where Mercury is known also as
“quicksilver”. The most obvious aspect of Perseus is his
internal defiance of the gods, especially Hades who was
responsible for his adopted family’s deaths. Perseus’ heart was
one of seeking vengeance. The “lessen” is learning how to “love
thine enemies” which translates to love thine no vengeance from
e-Nemesis. This is the “lessen”/lesson we all have to learn, not
just Perseus.

When one considers the depths of how far the ancients went to
preserve this information in allegorical and metaphorical ways,
it boggles the imagination.

Every my-story/mystery versus his-story paints all the pictures
you need to know to stop re-leasing your own Krakens and simply
turn it to stone. One look into Medusa’s eyes will do that. Once
the “serpent” has made it to your pineal, you will have the same
effect and render Cain into stone. Once you have the eyes and
ears of a Hermetic, the truth becomes as a waterfall. Take the
name of Perseus and LISTEN…Per = for from Latin, then add See
Us..”for to see us”..the letters are the spells and once you
begin to hear properly, as will you begin to see also. When a
ship begins to list, it is sinking so list-en your mind/ship
that has Cain at the helm. Also, do not forget that sounds are
in ALL languages or tongue pledges. Listen to serpent.

truth is that once something is “real-eyes-ed”, the spell is
broken and of no more effect on you.
Alas, I digress. Perseus was aided by Io, a mortal cursed with
forever youthfulness where she gets to watch those that she
loves die off as she will never age. Io is the creative right
virgin bride of thought. He is also aided by the Argos warriors,
all of whom with him, eventually perish but not before assisting
Perseus in many ways on their journey to save Andromeda, another
representative of the virgin thoughts and the kingdom of
Argos/the kingdom of heaven within....Ar-gent is French for
silver. Silver is the Moon, the reflections since no other metal
reflects light better where Mercury is known also as
“quicksilver”. The most obvious aspect of Perseus is his
internal defiance of the gods, especially Hades who was
responsible for his adopted family’s deaths. Perseus’ heart was
one of seeking vengeance. The “lessen” is learning how to “love
thine enemies” which translates to love thine no vengeance from
e-Nemesis. This is the “lessen”/lesson we all have to learn, not
just Perseus.
When one considers the depths of how far the ancients went to
preserve this information in allegorical and metaphorical ways,
it boggles the imagination.
Every my-story/mystery versus his-story paints all the pictures
you need to know to stop re-leasing your own Krakens and simply
turn it to stone. One look into Medusa’s eyes will do that. Once
the “serpent” has made it to your pineal, you will have the same
effect and render Cain into stone. Once you have the eyes and
ears of a Hermetic, the truth becomes as a waterfall. Take the
name of Perseus and LISTEN…Per = for from Latin, then add See
Us..”for to see us”..the letters are the spells and once you
begin to hear properly, as will you begin to see also. When a
ship begins to list, it is sinking so list-en your mind/ship
that has Cain at the helm. Also, do not forget that sounds are
in ALL languages or tongue pledges. Listen to serpent.
Sur, French for “on” and “pent” that equals head or mind. My
intuition immediately points to me needing to be using the over
or above, on top of mind, not the programmed “cerebellum”. This
was why Perseus had to retrieve the head of Medusa where all
other men had failed. Women were not allowed to enter her
“temple”, only men because it has been the mind of the masculine
intent that has to be fixed since creation, on HER own, works
just fine. Think of all your “pent” up e-motions now…As I have
shared so many times, e-motion means no motion or stopped…the
only thing that can stop the motion of love is the e-motions and
when they rule your head, Cain is cutting your throat and
laughing along with those that want to keep you all e-motion-
all….savvy ???

People have asked me why I haven’t written a book covering all
this stuff and the simple truth is that essays in the now work
better because by the time a whole book gets written it is
outdated from the now, past. This is a never ending story so it
must always ex-cyst in the now moment. Are you starting to grasp
the importance of the now yet? Time and space are the illusions
and only can be created in the consciousness of the all at
source. We are all the imagination of “ourself”, the one. It is
the separation that has us thinking we’re separate, nothing
else. I have long pondered why the two spirits were placed at
the top of the totem poles but I don’t any longer. Even the
native stories are metaphorical allegory and why all the
languages of natives from Celts to Maori, Aborigine to Native
“Americans” had to be literally erased. I dare you to read them
now with your new eyes and ears.

The Kraken is the crack-in you, the duality of left right, male
female. E-mails are called as such because only the female is
the ether in which creation happens. Thus the “ether” net. This
is the realm of Medusa. Many of you will still be squawking
about all this being coincidence but keep in mind. This is THEIR
language of spells, they made it, right down to the sounds of
the letters; each a word unto itself. This is why I look and
listen to every single sound, every letter in a “whirred” as an
independent sound etc. Even the “Psylla-Bulls”….yeah, just a
coincidence...sure, uh huh... They’re picking you off, letter by
letter, sound by damning sound until you awaken and slay your
Hades/Kraken/Cain mind. People have also said I need to write a
dictionary for this stuff…like I have the time for that. I am
one who would rather teach you the art of phi-shing where you
can catch all the phi-shhhhhh you want….yeah…another
coincidence. You’re Perseus, Hermes Trismegistus, Plato, Moses,
Joseph the pharoah, Jehoshua, aka Jesus (hey Zeus), Hari
Krishna, Buddha, Tiresias etc. if you would but shut up long
enough to hear the voice within.

Io was a gift from the “gods” for Perseus after he had slain the
Kraken/crack in himself. She could now come to him as a worthy
demi-god in the form of a purified man. Now he has someone to
listen to because SHE has appeared. She was slain at the height
of Perseus’ battle with the man who killed his mother for this
mans’ wife having unknowingly slept with Zeus and brought forth
a child of promise. His mother was Danae, Scottish for “don’t
know” and her husband was King Acrisius. I’ll leave his name for
the astute listeners and seers but it is pretty obvious ONCE you
start to really see and hear. “A” before a word/whirred means
“not” by the way. I’m glad that I was taught and LEARNED
grammar, something they don’t do so much anymore but I don’t
wonder why. The movie, “Clash of the Titans” leaves out a bit of
the real my-the so I’m posting a link for those that wish to
read a better version of it.

http://www.pantheon.org/articles/p/perseus.html

My own ability to see and hear is getting better these days and
it gets very frustrating trying to convey some of these concepts
that have taken a lifetime to gather but I will do my best. I
relish the contacts that tag me and let me know about their
“AHA!” moments…I have also quipped that I’d love to have a USB
port to download directly to everyone that asks but I already
know the download occurs with every aha moment we ALL have. We
are one and one truth destroys thousands of lies in one
epiphany. This is why I know we have already one. Given the
stark changes that were made in the movie versus the real story,
I can see why it was made the way it was. It is actually a
better rendering of the truths that we were all meant to get. In
short, the war is within each and every one of us. The Kraken,
for me, was merely the culmination of the entirety of the whore
thoughts manifest into one gigantic monster that even the gods
themselves feared. When it was turned to stone by looking into
the eyes of the serpent, it collapsed under its own weight and
was covered by the waters of truth.

I have read a few versions of the Perseus story and they all say
the same thing if you’re willing to dig beyond the mythical
surface to its truths hidden within. when considering words is the “Priesthood of the Illes” as brought forth by Jordan Maxwell, the author being another.

http://www.scribd.com/doc/34165385/Jordan-Maxwell-The-Priesthood-of-the-Illes

It is becoming increasingly difficult to write anything with all
the epiphanies showing up in the layers upon layers of the
words. Remember, don’t just write or see a whirred, LISTen to it
and bust it to pieces letter by letter, sound by sound. This is
how the spellings have been broken because with every “whirred”
revealed, so goes the spell. A fraud revealed is null and void,
nunc pro tunc, as if it never was. Unfortunately, you can’t use
THEIR words against them because they are THEIR spells. All we
can do is break the spell-bindings and rest upon the laurels of”
SILENCE IS GOLDEN”. Only in the silence can we hear our virgin
feminine, only in the silence can we thwart ALL spells. If you
are silent within, then you are silent without and the true
nature of self, the “ye are gods” part will begin to pre-sent
itself again. We are in the age of Aquarius now, there is
nothing that can stop this or slow it down for those who have
awakened. This is the separating of the wheat from the chaff,
where the sheaths of wheat will bow to you or you will bow down
to the master you have chosen. In the e-lites own words that “if
but one shall awaken, we are doomed”. Yup, doomed they are. For
even more clarity, doomed is defined as judged and, after all,
we are already judged by our actions that always follow intent,
never precede it.

I think it’s time for all of us to grow up and stop fighting in
their sandbox. They own all the Barbie dolls and Tonka trucks
and, quite frankly, they can have them since I have a better
universe to create where playing in it doesn’t have a debt
attached. When you can tell me you got this month’s rent from a
squirrel living in your attic, then you can say I’m wrong. Odd
that it’s only humans are required to pay another to live here.
What do the squirrels know that you don’t? Oh right…they know
who they are….they know of no Kraken they have to slay but then
one day, they will, once they evolve to human form. As above, so
below so please put down the weapons, the money and the spells
that have you fighting a shadow of yourself because it’s winning.

Re-lease the Kraken?..nope, and I’m not going to buy it
either..not a chance….much love, kate