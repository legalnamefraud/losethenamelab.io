import readingTime from "./node_modules/eleventy-plugin-reading-time/lib/reading-time.js";

export default async function (eleventyConfig) {
  eleventyConfig.addShortcode("year", () => `${new Date().getFullYear()}`);

  eleventyConfig.addPassthroughCopy({ "static": "static" });
  eleventyConfig.addPassthroughCopy({ "media": "media" });

  eleventyConfig.addShortcode("currentYear", () => `${new Date().getFullYear()}`);

  eleventyConfig.addShortcode("currentTime", () => {
    const now = new Date();
    const day = now.getDate();
    const month = now.getMonth() + 1;
    const year = now.getFullYear();
    return `${day}/${month}/${year}`;
  });
  
  eleventyConfig.addFilter('readingTime', readingTime);
}

export const config = {
  dir: {
    input: "./",
    output: "public",
  },
};
